# Hugb2020Template

This is a template project that contains the structure for the project work in T-303-HUGB Software Engineering, Fall 2020.

Please make sure to read the [Code of Conduct](https://gitlab.com/grischal/hugb2020template/-/blob/master/code-of-conduct.md).
